Page Navigation Extension for Mecha
===================================

Release Notes
-------------

### 1.3.0

 - Added `PagerSteps\Pages` and `PagerTitle\Page` class to make reusable custom pagination.
 - Changed `self::pager('step')` to `self::pager('steps')`.
 - Fixed pagination bug in the tags page (#2)

### 1.2.3

 - Fixed a bug for pagination URL in home page.

### 1.2.2

 - Fixed bugs with other extensions.

### 1.2.1

 - Added `pager/title` layout.
